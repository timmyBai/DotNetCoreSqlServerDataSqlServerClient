﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Models
{
    /// <summary>
    /// 取得學員資料模型
    /// </summary>
    public class StudentModels
    {
        /// <summary>
        /// id
        /// </summary>
        public Guid id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// 名字
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";
    }

    /// <summary>
    /// 更新學員資料模型
    /// </summary>
    public class UpdateStudentModels
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";
    }
}
