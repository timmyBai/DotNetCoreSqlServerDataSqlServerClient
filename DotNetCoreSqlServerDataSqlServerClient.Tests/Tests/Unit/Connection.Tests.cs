using DotNetCoreSqlServerDataSqlServerClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Tests.Unit
{
    [TestClass]
    public class ConnectionTests
    {
        /// <summary>
        /// 測試資料庫連線狀態
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlServerConnectionSuccess()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();

                Assert.IsTrue(conn.State() == true);
            }
        }

        /// <summary>
        /// 資料庫連線狀態 Async 方法
        /// </summary>
        /// <returns></returns>
        [Test]
        [TestMethod]
        public async Task TestSqlServerConnectionSuccessAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();

                Assert.IsTrue(conn.StateAsync().Result == true);
            }
        }

        /// <summary>
        /// 測試新增資料庫連線狀態
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlServerCreateConnectSuccess()
        {
            using(DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                var DotNetCoreMySqlDataMySqlClientDB = conn.CreateConnect();

                Assert.IsTrue(DotNetCoreMySqlDataMySqlClientDB.State == ConnectionState.Open);
            }
        }

        /// <summary>
        /// 測試新增資料庫連線狀態
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlCreateConnectionSuccessAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                var DotNetCoreMySqlDataMySqlClientDB = await conn.CreateConnectAsync();

                Assert.IsTrue(DotNetCoreMySqlDataMySqlClientDB.State == ConnectionState.Open);
            }
        }

        /// <summary>
        /// 測試資料庫停止連線與釋放資源
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlServerConnectionClose()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.Close();
                conn.Dispose();

                Assert.IsTrue(conn.State() == false);
            }
        }

        /// <summary>
        /// 測試資料庫停止連線與釋放資源 Async 方法
        /// </summary>
        /// <returns></returns>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionCloseAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();
                await conn.CloseAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(conn.StateAsync().Result == false);
            }
        }

        /// <summary>
        /// 測試連線指定資料庫
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlServerConnectionDatabase()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect("test_db");

                Assert.IsTrue(conn.State() == true);
            }
        }

        /// <summary>
        /// 測試連線指定資料庫 Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestSqlServerConnectionDatabaseAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync("test_db");

                Assert.IsTrue(conn.StateAsync().Result == true);
            }
        }
    }
}
