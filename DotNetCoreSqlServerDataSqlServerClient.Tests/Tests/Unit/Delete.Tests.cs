using DotNetCoreSqlServerDataSqlServerClient.Tests.Models;
using DotNetCoreSqlServerDataSqlServerClient.Tests.Utils;
using Microsoft.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Tests.Unit
{
    [TestClass]
    public class DeleteTests
    {
        /// <summary>
        /// 測試資料庫刪除資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionDeleteOnlyDataForDapper()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                var result = conn.Query<StudentModels>(sqlStr);

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                conn.Execute(sqlStr, new
                {
                    id = result.ToList()[0].id
                });

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫刪除資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionDeleteOnlyDataForDapperAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                var result = await conn.QueryAsync<StudentModels>(sqlStr);

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                var deleteRow = await conn.ExecuteAsync(sqlStr, new
                {
                    id = result.ToList()[0].id
                });

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(deleteRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫刪除資料(SqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionDeleteOnlyDataSqlCommand()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                DataTable result = conn.Query(sqlStr);

                Guid id = Guid.Parse(result.Rows[0]["id"].ToString());

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                conn.Execute(sqlStr, new SqlParameter("@id", id));

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫刪除資料(SqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionDeleteOnlyDataMysqlCommandAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                DataTable result = await conn.QueryAsync(sqlStr);

                Guid id = Guid.Parse(result.Rows[0]["id"].ToString());

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                var deleteRow = await conn.ExecuteAsync(sqlStr, new SqlParameter("@id", id));

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(deleteRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試生成 Delete Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlConnectionGenerateDeleteSql()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                var result = conn.Query<StudentModels>(sqlStr);

                var param = new
                {
                    id = result.FirstOrDefault().id
                };

                conn.GenerateDeleteSql(out sqlStr, "student_basic", param);

                conn.Execute(sqlStr, param);

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }
    }
}
