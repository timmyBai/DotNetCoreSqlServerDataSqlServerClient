using DotNetCoreSqlServerDataSqlServerClient.Tests.Models;
using DotNetCoreSqlServerDataSqlServerClient.Tests.Utils;
using Microsoft.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Tests.Unit
{
    [TestClass]
    public class InsertTests
    {
        /// <summary>
        /// 測試資料庫新增資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlServerConnectionInsertOnlyDataForDapper()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "INSERT INTO student_basic (id, name, gender) VALUES (@id, @name, @gender)";

                conn.Execute(sqlStr, new
                {
                    id = Guid.NewGuid(),
                    name = "王測試",
                    gender = "女"
                });

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫新增資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestSqlServerConnectionInsertOnlyDataForDapperAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "INSERT INTO student_basic (id, name, gender) VALUES (@id, @name, @gender)";

                var insertRow = await conn.ExecuteAsync(sqlStr, new
                {
                    id = Guid.NewGuid(),
                    name = "王測試",
                    gender = "女"
                });

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(insertRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫新增資料(SqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlServerConnectionInsertOnlyForSqlCommand()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "INSERT INTO student_basic (id, name, gender) VALUES (@id, @name, @gender)";

                conn.Execute(sqlStr,
                    new SqlParameter("@id", Guid.NewGuid()),
                    new SqlParameter("@name", "方測試"),
                    new SqlParameter("@gender", "男")
                );

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫新增資料 (SqlCommand) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestSqlServerConnectionInsertOnlyForMysqlCommandAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "INSERT INTO student_basic (id, name, gender) VALUES (@id, @name, @gender)";

                var insertRow = await conn.ExecuteAsync(sqlStr,
                    new SqlParameter("@id", Guid.NewGuid()),
                    new SqlParameter("@name", "方測試"),
                    new SqlParameter("@gender", "男")
                );

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(insertRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試生成 Insert Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlConnectionGenerateInsertSql()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "";

                conn.GenerateInsertSql<StudentModels>(out sqlStr, "student_basic");

                conn.Execute(sqlStr, new
                {
                    id = Guid.NewGuid(),
                    gender = "男",
                    name = "方測試"
                });

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }
    }
}
