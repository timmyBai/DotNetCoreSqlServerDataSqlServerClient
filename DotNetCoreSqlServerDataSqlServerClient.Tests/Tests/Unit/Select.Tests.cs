using DotNetCoreSqlServerDataSqlServerClient.Tests.Models;
using DotNetCoreSqlServerDataSqlServerClient.Tests.Utils;
using Microsoft.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Tests.Unit
{
    [TestClass]
    public class SelectTests
    {
        /// <summary>
        /// 測試資料庫取得資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionOnlyDataForDapper()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                var result = conn.Query<StudentModels>(sqlStr);

                conn.Dispose();

                Assert.IsTrue(result.Count() > 0);
            }
        }

        /// <summary>
        /// 測試資料庫取得資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionOnlyDataForDapperAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                var result = conn.QueryAsync<StudentModels>(sqlStr).Result;

                Assert.IsTrue(result.Count() > 0);
            }
        }

        /// <summary>
        /// 測試資料庫取得資料(SqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionGetOnlyDataForDataTable()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();

                string sqlStr = "SELECT * FROM student_basic WHERE gender = @gender AND name LIKE @name";
                DataTable result = conn.Query(sqlStr,
                    new SqlParameter("@name", "王%"),
                    new SqlParameter("@gender", "男")
                );

                conn.Dispose();

                Assert.IsTrue(result.Rows.Count > 0);
            }
        }

        /// <summary>
        /// 測試資料庫取得資料(SqlCommand) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionGetOnlyDataForDataTableAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();

                string sqlStr = "SELECT * FROM student_basic WHERE name LIKE @name";

                DataTable result = await conn.QueryAsync(sqlStr,
                    new SqlParameter("@name", "王%")
                );

                await conn.DisposeAsync();

                Assert.IsTrue(result.Rows.Count > 0);
            }
        }

        /// <summary>
        /// 測試生成 Select Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlConnectionGenerateSelectSql()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();

                string sqlStr = "";

                var param = new
                {
                    name = "王測試"
                };

                conn.GenerateSelectSql<StudentModels>(out sqlStr, "student_basic", param);

                var result = conn.Query<StudentModels>(sqlStr, param);

                conn.Dispose();

                Assert.IsTrue(result.Count() > 0);
            }
        }
    }
}
