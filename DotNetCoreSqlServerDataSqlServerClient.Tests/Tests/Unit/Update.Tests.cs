using DotNetCoreSqlServerDataSqlServerClient.Tests.Models;
using DotNetCoreSqlServerDataSqlServerClient.Tests.Utils;
using Microsoft.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Tests.Unit
{
    [TestClass]
    public class UpdateTests
    {
        /// <summary>
        /// 測試資料庫更新資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionUpdateOnlyDataForDapper()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                var result = conn.Query<StudentModels>(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                conn.Execute(sqlStr, new
                {
                    gender = "男",
                    id = result.FirstOrDefault().id
                });

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫更新資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionUpdateOnlyDataForDapperAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                var result = await conn.QueryAsync<StudentModels>(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                var updateRow = await conn.ExecuteAsync(sqlStr, new
                {
                    gender = "男",
                    id = result.FirstOrDefault().id
                });

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(updateRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫更新資料(SqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionUpdateOnlyForSqlCommand()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                DataTable result = conn.Query(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                conn.Execute(sqlStr,
                    new SqlParameter("@gender", "男"),
                    new SqlParameter("@id", result.Rows[0]["id"])
                );

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫更新資料(SqlCommand) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionUpdateOnlyForMysqlCommandAsync()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT TOP 3 * FROM student_basic";

                DataTable result = await conn.QueryAsync(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                var updateRow = await conn.ExecuteAsync(sqlStr,
                    new SqlParameter("@gender", "男"),
                    new SqlParameter("@id", result.Rows[0]["id"])
                );

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(updateRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試生成 Update Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public void TestSqlConnectionGenerateUpdateSqlPerformanceVersion()
        {
            using (DotNetCoreSqlServerConnection conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT TOP 1 * FROM student_basic";

                var result = conn.Query<StudentModels>(sqlStr).SingleOrDefault();

                UpdateStudentModels updateStudentModels = new UpdateStudentModels();
                updateStudentModels.name = "藍測試";
                updateStudentModels.gender = result.gender == "男" ? "女" : "男";

                var param = new
                {
                    id = result.id,
                    name = updateStudentModels.name,
                    gender = result.gender
                };

                conn.GenerateUpdateSql(result, updateStudentModels, out sqlStr, "student_basic", new
                {
                    id = result.id
                });

                conn.Execute(sqlStr, param);

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(conn.RowsCount == 1);
            }
        }
    }
}
