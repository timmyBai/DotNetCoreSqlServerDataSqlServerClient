using Microsoft.Data.SqlClient;
using System.Data;
using System.Reflection;
using Dapper;

namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Utils
{
    public sealed class DotNetCoreSqlServerConnection : DotNetCoreSqlServerSetting, IDisposable
    {
        /// <summary>
        /// SQL Server 連線 class
        /// </summary>
        private SqlConnection? Connection = null;

        /// <summary>
        /// 交易事件
        /// </summary>
        private SqlTransaction? Transaction = null;

        /// <summary>
        /// 連線字串
        /// </summary>
        private string ConnectionString = "";

        /// <summary>
        /// 變更資料列數
        /// </summary>
        public int RowsCount = 0;

        /// <summary>
        /// SQL Server 連線初始化
        /// </summary>
        public DotNetCoreSqlServerConnection()
        {
            ConnectionString = "";
            Connection = null;
            RowsCount = 0;
        }

        /// <summary>
        /// 初始化連線
        /// </summary>
        /// <param name="databaseName">資料庫名稱</param>
        public void Connect(string databaseName = "")
        {
            if (!string.IsNullOrWhiteSpace(databaseName))
            {
                DataBase = databaseName;
            }

            ConnectionString = GetConnectionString();
            Connection = new SqlConnection(ConnectionString);
            RowsCount = 0;
            this.Open();
        }

        /// <summary>
        /// 初始化連線 Async 方法
        /// </summary>
        /// <param name="databaseName">資料庫名稱</param>
        /// <returns></returns>
        public async Task ConnectAsync(string databaseName = "")
        {
            if (!string.IsNullOrWhiteSpace(databaseName))
            {
                DataBase = databaseName;
            }

            ConnectionString = GetConnectionString();
            Connection = new SqlConnection(ConnectionString);
            RowsCount = 0;
            await this.OpenAsync();
        }

        /// <summary>
        /// 初始化連線
        /// </summary>
        /// <param name="databaseName">資料庫名稱</param>
        /// <returns></returns>
        public IDbConnection CreateConnect(string databaseName = "")
        {
            if (!string.IsNullOrWhiteSpace(databaseName))
            {
                DataBase = databaseName;
            }

            ConnectionString = GetConnectionString();
            Connection = new SqlConnection(ConnectionString);
            RowsCount = 0;
            this.Open();
            return Connection;
        }

        /// <summary>
        /// 初始化連線 Async 方法
        /// </summary>
        /// <param name="databaseName">資料庫名稱</param>
        /// <returns></returns>
        public async Task<IDbConnection> CreateConnectAsync(string databaseName = "")
        {
            if (!string.IsNullOrWhiteSpace(databaseName))
            {
                DataBase = databaseName;
            }

            ConnectionString = GetConnectionString();
            Connection = new SqlConnection(ConnectionString);
            RowsCount = 0;
            await this.OpenAsync();
            return await Task.FromResult<IDbConnection>(Connection);
        }

        /// <summary>
        /// 開啟資料庫
        /// </summary>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public void Open()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            Connection.Open();
        }

        /// <summary>
        /// 開啟資料庫 Async 方法
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public async Task OpenAsync()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            await Connection.OpenAsync();
        }

        /// <summary>
        /// 關閉資料庫
        /// </summary>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public void Close()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                Connection.Close();
            }
        }

        /// <summary>
        /// 關閉資料庫 Async 方法
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public async Task CloseAsync()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                await Connection.CloseAsync();
            }
        }

        /// <summary>
        /// 交易請求
        /// </summary>
        public void BeginTransaction()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            Transaction = Connection.BeginTransaction();
        }

        /// <summary>
        /// 交易請求 Async 方法
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public async Task BeginTransactionAsync()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            Transaction = (SqlTransaction?)await Connection.BeginTransactionAsync();
        }

        /// <summary>
        /// 資料庫連線狀態
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public bool State()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Connection.State == ConnectionState.Open)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 資料庫連線狀態 Async 方法
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public async Task<bool> StateAsync()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Connection.State == ConnectionState.Open)
            {
                return await Task.FromResult<bool>(true);
            }

            return await Task.FromResult<bool>(false);
        }

        /// <summary>
        /// 釋放資料庫資源
        /// </summary>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public void Dispose()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                Connection.Dispose();
            }
        }

        /// <summary>
        /// 釋放資料庫資源 Async 方法
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        public async Task DisposeAsync()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                await Connection.DisposeAsync();
            }
        }

        /// <summary>
        /// 提交資料
        /// </summary>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        public void Commit()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            Transaction?.Commit();
        }

        /// <summary>
        /// 提交資料 Async 方法
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        public async Task CommitAsync()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            await Transaction.CommitAsync();
        }

        /// <summary>
        /// 撤銷資料
        /// </summary>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        public void Rollback()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            Transaction?.Rollback();
        }

        /// <summary>
        /// 撤銷資料 Async 方法
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        public async Task RollbackAsync()
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            await Transaction.RollbackAsync();
        }

        /// <summary>
        /// 取得資料庫資料(For Dapper)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="param">不可破壞物件</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public IEnumerable<T> Query<T>(string sqlStr = "", object? param = null)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            try
            {
                var query = Connection.Query<T>(sqlStr, param, Transaction);
                RowsCount = query.Count();

                return query;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 取得資料庫資料(For Dapper) Async 方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="param">不可破壞物件</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public async Task<IEnumerable<T>> QueryAsync<T>(string sqlStr = "", object? param = null)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            try
            {
                var query = await Connection.QueryAsync<T>(sqlStr, param, Transaction);
                RowsCount = query.Count();

                return await Task.FromResult<IEnumerable<T>>(query);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 取得資料庫資料(For SqlCommand)
        /// </summary>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="param">不可破壞物件</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public DataTable Query(string sqlStr = "", params SqlParameter[] param)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            try
            {
                SqlCommand cmd = new SqlCommand(sqlStr, Connection, Transaction);

                cmd.CommandText = sqlStr;
                param.ToList().ForEach((item) =>
                {
                    cmd.Parameters.Add(item);
                });

                SqlDataReader reader = cmd.ExecuteReader();

                DataTable dt = new DataTable();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    dt.Columns.Add(reader.GetName(i));
                }

                while (reader.Read())
                {
                    DataRow newRow = dt.NewRow(); // 建立新的 DataRow 物件

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        newRow[reader.GetName(i)] = reader.GetValue(i).ToString();
                    }

                    dt.Rows.Add(newRow);
                }

                RowsCount = dt.Rows.Count;

                reader.Close();

                return dt;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 取得資料庫資料(For SqlCommand) Async 方法
        /// </summary>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="param">不可破壞物件</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public async Task<DataTable> QueryAsync(string sqlStr = "", params SqlParameter[] param)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            try
            {
                SqlCommand cmd = new SqlCommand(sqlStr, Connection, Transaction);

                cmd.CommandText = sqlStr;
                param?.ToList().ForEach((item) =>
                {
                    cmd.Parameters.Add(item);
                });

                SqlDataReader reader = (SqlDataReader)await cmd.ExecuteReaderAsync();

                DataTable dt = new DataTable();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    dt.Columns.Add(reader.GetName(i));
                }

                while (await reader.ReadAsync())
                {
                    DataRow newRow = dt.NewRow(); // 建立新的 DataRow 物件

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        newRow[reader.GetName(i)] = reader.GetValue(i).ToString();
                    }

                    dt.Rows.Add(newRow);
                }

                RowsCount = dt.Rows.Count;

                await reader.CloseAsync();

                return await Task.FromResult<DataTable>(dt);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 變更資料庫資料(For Dapper)
        /// </summary>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="param">不可破壞物件</param>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public void Execute(string sqlStr, object? param = null)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            try
            {
                int changeRow = Connection.Execute(sqlStr, param, Transaction);
                RowsCount = changeRow;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 變更資料庫資料(For Dapper) Async 方法
        /// </summary>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="param">不可破壞物件</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public async Task<int> ExecuteAsync(string sqlStr, object? param = null)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            try
            {
                RowsCount = await Connection.ExecuteAsync(sqlStr, param, Transaction);

                return await Task.FromResult<int>(RowsCount);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 變更資料庫資料(For SqlCommand)
        /// </summary>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="param">不可破壞物件</param>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public void Execute(string sqlStr = "", params SqlParameter[] param)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            try
            {
                SqlCommand cmd = new SqlCommand(sqlStr, Connection, Transaction);

                cmd.CommandText = sqlStr;
                param?.ToList().ForEach((item) =>
                {
                    cmd.Parameters.Add(item);
                });

                RowsCount = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 變更資料庫資料(For SqlCommand) Async 方法
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">The database is not online.</exception>
        /// <exception cref="InvalidOperationException">No transaction occurred.</exception>
        /// <exception cref="InvalidOperationException">ex.Message</exception>
        public async Task<int> ExecuteAsync(string sqlStr = "", params SqlParameter[] param)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("The database is not online.");
            }

            if (Transaction == null)
            {
                throw new InvalidOperationException("No transaction occurred.");
            }

            try
            {
                SqlCommand cmd = new SqlCommand(sqlStr, Connection, Transaction);

                cmd.CommandText = sqlStr;
                param?.ToList().ForEach((item) =>
                {
                    cmd.Parameters.Add(item);
                });

                RowsCount = await cmd.ExecuteNonQueryAsync();

                return await Task.FromResult<int>(RowsCount);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <summary>
        /// 生成 Select Sql 語法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="whereKey">條件物件</param>
        public void GenerateSelectSql<T>(out string sqlStr, string tableName, object whereKey = null)
        {
            sqlStr = "SELECT ";

            var properties = typeof(T).GetProperties();

            for (int i = 0; i < properties.Length; i++)
            {
                if (i < properties.Length - 1)
                {
                    sqlStr = sqlStr + $"{properties[i].Name}, ";
                }
                else
                {
                    sqlStr = sqlStr + $"{properties[i].Name}";
                }
            }

            sqlStr = sqlStr + $" FROM {tableName} ";

            if (whereKey != null)
            {
                sqlStr = sqlStr + "WHERE ";

                var objWhereKey = whereKey.GetType().GetProperties();

                for (int i = 0; i < objWhereKey.Length; i++)
                {
                    if (i < objWhereKey.Length - 1)
                    {
                        sqlStr = sqlStr + $"{objWhereKey[i].Name} = @{objWhereKey[i].Name} AND ";
                    }
                    else
                    {
                        sqlStr = sqlStr + $"{objWhereKey[i].Name} = @{objWhereKey[i].Name}";
                    }
                }
            }
        }

        /// <summary>
        /// 生成 Insert Sql 語法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="tableName">資料表名稱</param>
        public void GenerateInsertSql<T>(out string sqlStr, string tableName)
        {
            sqlStr = $"INSERT INTO {tableName} (";

            var properties = typeof(T).GetProperties();

            // 生成 insert values 字串
            for (int i = 0; i < properties.Length; i++)
            {
                if (i < properties.Length - 1)
                {
                    sqlStr = sqlStr + properties[i].Name + ", ";
                }
                else
                {
                    sqlStr = sqlStr + properties[i].Name;
                }
            }

            sqlStr = sqlStr + ") VALUES (";

            // 生成 paramter 字串
            for (int i = 0; i < properties.Length; i++)
            {
                if (i < properties.Length - 1)
                {
                    sqlStr = sqlStr + $"@{properties[i].Name}, ";
                }
                else
                {
                    sqlStr = sqlStr + $"@{properties[i].Name}";
                }
            }

            sqlStr = sqlStr + ")";
        }

        /// <summary>
        /// 生成 Update Sql 語法
        /// </summary>
        /// <param name="oldObject">舊資料物件</param>
        /// <param name="newObject">新資料物件</param>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="whereKey">條件物件</param>
        public void GenerateUpdateSql(object oldObject, object newObject, out string sqlStr, string tableName, object whereKey)
        {
            sqlStr = $"UPDATE {tableName} SET ";

            PropertyInfo[] oldProperInfo = oldObject.GetType().GetProperties();
            PropertyInfo[] newProperInfo = newObject.GetType().GetProperties();
            PropertyInfo oldProp;
            PropertyInfo newProp;

            bool dateChange = false;

            for (int i = 0; i < newProperInfo.Count(); i++)
            {
                newProp = newProperInfo[i];
                oldProp = oldProperInfo[i];

                if (newProp != null && oldProp != null)
                {
                    object newPropGetValue = newProp.GetValue(newObject, null);
                    object oldPropGetValue = oldProp.GetValue(oldObject, null);

                    if (newPropGetValue != null && oldPropGetValue != null)
                    {
                        if (!newPropGetValue.Equals(oldPropGetValue))
                        {
                            dateChange = true;
                        }
                    }
                    else if (newPropGetValue != null && oldPropGetValue == null)
                    {
                        dateChange = true;
                    }
                    else if (newPropGetValue == null && oldPropGetValue != null)
                    {
                        dateChange = true;
                    }
                }

                if (dateChange)
                {
                    sqlStr = sqlStr + newProp.Name + $"= @{newProp.Name}, ";
                }
            }

            sqlStr = sqlStr.Substring(0, sqlStr.Length - 2);
            sqlStr = sqlStr + " WHERE ";

            var objWhereKey = whereKey.GetType().GetProperties();

            if (objWhereKey != null)
            {
                for (int i = 0; i < objWhereKey.Length; i++)
                {
                    if (i < objWhereKey.Length - 1)
                    {
                        sqlStr = sqlStr + objWhereKey[i].Name + $"= @{objWhereKey[i].Name} AND ";
                    }
                    else
                    {
                        sqlStr = sqlStr + objWhereKey[i].Name + $"= @{objWhereKey[i].Name}";
                    }
                }
            }
        }

        /// <summary>
        /// 生成 Delete Sql 語法
        /// </summary>
        /// <param name="sqlStr">SQL 語法</param>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="whereKey">條件物件</param>
        public void GenerateDeleteSql(out string sqlStr, string tableName, object whereKey)
        {
            sqlStr = $"DELETE FROM {tableName} WHERE ";

            var objWhereKey = whereKey.GetType().GetProperties();

            for (int i = 0; i < objWhereKey.Length; i++) {
                if (i < objWhereKey.Length - 1)
                {
                    sqlStr = sqlStr + objWhereKey[i].Name + $"= @{objWhereKey[i].Name} AND ";
                }
                else
                {
                    sqlStr = sqlStr + objWhereKey[i].Name + $"= @{objWhereKey[i].Name}";
                }
            }
        }
    }
}
