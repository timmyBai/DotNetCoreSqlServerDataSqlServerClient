namespace DotNetCoreSqlServerDataSqlServerClient.Tests.Utils
{
    public class DotNetCoreSqlServerSetting
    {
        /// <summary>
        /// 主機位置
        /// </summary>
        public string Host = "localhost";

        /// <summary>
        /// 主機連接埠
        /// </summary>
        public string Port = "1433";

        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string User = "sa";

        /// <summary>
        /// 密碼
        /// </summary>
        public string Password = "123456";

        /// <summary>
        /// 資料庫名稱
        /// </summary>
        public string DataBase = "DotNetCoreSqlServerDataSqlServerClient";

        /// <summary>
        /// 是否略過 SSL 憑證
        /// </summary>
        public bool TrustServerCertificate = true;

        /// <summary>
        /// 連線逾時(單位: 秒)
        /// </summary>
        public int ConnectionTimeout = 3;

        public string GetConnectionString()
        {
            return string.Format(
                "Server={0},{1}; User={2}; Password={3}; Database={4}; TrustServerCertificate={5}; Connection Timeout={6}",
                Host,
                Port,
                User,
                Password,
                DataBase,
                TrustServerCertificate,
                ConnectionTimeout
            );
        }
    }
}
