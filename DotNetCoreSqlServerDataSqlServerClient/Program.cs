using DotNetCoreSqlServerDataSqlServerClient.Utils;

public class Program
{
    private static DotNetCoreSqlServerConnection? conn;

    public static void Main()
    {
        try
        {
            using (conn = new DotNetCoreSqlServerConnection())
            {
                conn.Connect();

                Console.WriteLine(conn.State());
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }
}