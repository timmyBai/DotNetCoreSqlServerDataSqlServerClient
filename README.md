# DotNetCoreSqlServerDataSqlServerClient

<div>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet" target="_blank">
        <img src="https://img.shields.io/badge/C%23-8.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet" target="_blank">
        <img src="https://img.shields.io/badge/.NetCore-8.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet" target="_blank">
        <img src="https://img.shields.io/badge/ConsoleApplication-8.0.0-blue">
    </a>
</div>

## 簡介

DotNetCoreSqlServerDataSqlServerClient 是一個建構共用連線 Sql Server 資料庫 架構解決方案，他基於 C#、.Net Core 6 實現，他使用最新 Mysql 資料庫，內置共用連線方法、資料庫語法執行，跨平台相容性，輕量級、高效能且模組化，提供了許多資料庫處理方法，他可以幫助你快速搭建 Sql 原型，可以幫助你解決許多需求。

## 功能

```tex
- 初始化連線 ( Connect )
- 初始化連線 Async 方法 ( ConnectAsync )
- 初始化連線 ( CreateConnect )
- 初始化連線 Async 方法 ( CreateConnectAsync )
- 開啟資料庫 ( Open )
- 開啟資料庫 Async 方法 ( OpenAsync )
- 關閉資料庫 ( Close
- 關閉資料庫 Async 方法 ( CloseAsync )
- 交易請求 ( BeginTransaction )
- 交易請求 Async 方法 ( BeginTransactionAsync )
- 資料庫連線狀態 ( State )
- 資料庫連線狀態 Async 方法 ( StateAsync )
- 釋放資料庫資源 ( Dispose )
- 釋放資料庫資源 Async 方法 ( DisposeAsync )
- 提交資料 ( Commit )
- 提交資料 Async 方法 ( CommitAsync )
- 撤銷資料 ( Rollback )
- 撤銷資料 Async 方法 ( RollbackAsync )
- 取得資料庫資料(For Dapper) ( Query )
- 取得資料庫資料(For Dapper) Async 方法 ( QueryAsync )
- 取得資料庫資料(For SqlCommand) ( Query )
- 取得資料庫資料(For SqlCommand) Async 方法 ( QueryAsync )
- 變更資料庫資料(For Dapper) ( Execute )
- 變更資料庫資料(For Dapper) Async 方法 ( ExecuteAsync )
- 變更資料庫資料(For SqlCommand) ( Execute )
- 變更資料庫資料(For SqlCommand) Async 方法 ( ExecuteAsync )
- 生成 Select Sql 語法 ( GenerateSelectSql )
- 生成 Insert Sql 語法 ( GenerateInsertSql )
- 生成 Update Sql 語法 ( GenerateUpdateSql )
- 生成 Delete Sql 語法 ( GenerateDeleteSql )
```

## 開發

```git
git clone https://gitlab.com/timmyBai/DotNetCoreSqlServerDataSqlServerClient.git
```

## 測試

```tex
dotnet test DotNetCoreSqlServerDataSqlServerClient.Tests
```

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
